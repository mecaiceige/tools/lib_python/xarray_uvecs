.. xarray_uvecs documentation master file, created by
   sphinx-quickstart on Mon Aug  2 14:06:30 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xarray_uvecs's documentation!
========================================

``xarray_uvecs`` is a `xarray <http://xarray.pydata.org/en/stable/>`_ accesors to work on **U**nit **Vec**tor **Symmetric**. It means **v** and **-v** have the same meaning.

In this documentation detail of the functions are given. For quick start script to use this accesors we recommend to have a look to the `AITA Book <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/AITA-book/docs/intro.html>`_ where ``xarray_uvecs`` is used.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Installation
============

From repository
***************

.. code:: bash
    
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs
    cd xarray_uvecs
    python -m pip install -r requirements.txt
    pip install -e .

Functions Overview
==================

.. toctree::
    :maxdepth: 0
    :numbered: 
    
    func_o


Contact
=======
:Author: Thomas Chauve
:Contact: thomas.chauve@univ-grenoble-alpes.fr

:Organization: `IGE <https://www.ige-grenoble.fr/>`_
:Status: This is a "work in progress"
:Version: 0.1.15

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
