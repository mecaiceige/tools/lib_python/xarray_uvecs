# Librairy xarray_uvecs

[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs/-/blob/main/LICENSE)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs/-/commits/main)
[![PyPI version](https://badge.fury.io/py/xarrayuvecs.svg)](https://badge.fury.io/py/xarrayuvecs)
[![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_uvecs/)

This package is meant to be use with [xarray](https://github.com/pydata/xarray)
It is a wrapper for 3D unit vector that are symmetric. It means that **x**=**-x**

## Installation

### From pypi

Run this command in your environment :

`pip install xarrayuvecs`

### From main repository

You should clone the environnement.

`git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs`

Go in the folder.

`cd xarray_uvec`

Use pip to install it.

`pip install -e .`
